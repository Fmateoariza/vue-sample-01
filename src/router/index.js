import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
Vue.use(VueRouter)

const routes = [ 
  {
    path: '/',
    name: 'MyCars',
    
    component: () => import(/* webpackChunkName: "MyCars" */ '../views/MyCars.vue'),
    meta: {protectedRoute: true}
  },
  {
    path: '/register',
    name: 'Register',
    
    component: () => import(/* webpackChunkName: "Register" */ '../views/Register.vue')
  },
  {
    path: '/login',
    name: 'Login',
    
    component: () => import(/* webpackChunkName: "Login" */ '../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  // console.log(to.meta.rutaProtegida)
  if (to.meta.protectedRoute) {
    let user = localStorage.getItem('user');
    
    user = !user ? store.getters.authenticatedUser : user;

    if (user) {
      next()
    } else {
      next('/login')
    }
  } else {
    next()
  }
})
export default router
