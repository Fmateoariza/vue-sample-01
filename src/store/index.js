import Vue from 'vue'
import Vuex from 'vuex'
import router from '../router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cars: [],
    car: {
      id:     '',
      brand:  '',
      model:  '',
      price:  0,
    },
    user: null
  },
  mutations: {
    load(state, payload) {
      state.cars = payload;
    },
    set(state, payload) {
      state.cars.push(payload);
    },
    delete(state, payload){
      state.cars = state.cars.filter(item => item.id !== payload);
    },
    setUser(state, payload) {
      state.user = payload;
    },
    
  },
  actions: {    
    async loadFromDatabase({ commit, state }) {
      if (localStorage.getItem('user')) {
        commit('setUser', JSON.parse(localStorage.getItem('user')))
      } else {
        return commit('setUser', null)
      }
      try {
        const res = await fetch(`https://ult-cars-api-default-rtdb.europe-west1.firebasedatabase.app/cars/${state.user.localId}.json?auth=${state.user.idToken}`)
        const dataDB = await res.json()
        const arrayCars = []
        for (let id in dataDB){
          arrayCars.push(dataDB[id]);
        }
        commit('load', arrayCars)

      } catch (error) {
        console.log(error);
      }
    },
    async setCars({ commit, state }, car) {
      try {
        const res = await fetch(`https://ult-cars-api-default-rtdb.europe-west1.firebasedatabase.app/cars/${state.user.localId}/${car.id}.json?auth=${state.user.idToken}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(car)
        })

        const dataDB = await res.json()
        
        commit('set', car);
        console.log(dataDB);

      } catch (error) {
        console.log(error);
      }

    },
    async deleteCars({ commit, state }, id) {
      try {
        await fetch(`https://ult-cars-api-default-rtdb.europe-west1.firebasedatabase.app/cars/${state.user.localId}/${id}.json?auth=${state.user.idToken}`, {
          method: 'DELETE',
        })
        commit('delete', id);
      } catch (error) {
        console.log(error);
      }     
    },
    async registerUser({ commit, state }, user) {
      try {
        const res = await fetch('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAC7qerrxjS5EKjnKNoPDMEJrlKAdQIlDw', {
          method: 'POST',
          body: JSON.stringify({
            email: user.email,
            password: user.password,
            returnSecureToken: true
          })
        })
        const userDB = await res.json()
        console.log(userDB)
        if (userDB.error) {
          console.log(userDB.error);
          return
        }
        commit('setUser', userDB);
        router.push('/');
        localStorage.setItem('user', JSON.stringify(userDB));
      } catch (error) {
        console.log(error);
      }
    },
    async loginUser({ commit }, user) {
      try {
        const res = await fetch('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAC7qerrxjS5EKjnKNoPDMEJrlKAdQIlDw', {
          method: 'POST',
          body: JSON.stringify({
            email: user.email,
            password: user.password,
            returnSecureToken: true
          })
        })
        const userDB = await res.json()
        console.log('userDB', userDB)
        if (userDB.error) {
          return console.log(userDB.error)
        }
        commit('setUser', userDB)
        router.push('/')
        localStorage.setItem('user', JSON.stringify(userDB));
      } catch (error) {
        console.log(error)
      }
    },
    closeSession({ commit }){
      commit('setUser', null);
      router.push('/login');
      localStorage.removeItem('user');
    }
  },
  getters: {
    authenticatedUser(state) {
      return !!state.user
    }
  },
  modules: {
  }
})
